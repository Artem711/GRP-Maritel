export const SET_CATEGORIES = "CATEGORIES/SET_CATEGORIES";
export const SET_SPEC_CATEGORIES = "CATEGORIES/SET_SPEC_CATEGORIES";
export const SET_BACKGROUND = "BACKGROUND/SET_BACKGROUND";
export const SET_BACKGROUND_COVER = "BACKGROUND/SET_BACKGROUND_COVER";
export const SET_PRODUCTS = "PRODUCTS/SET_PRODUCTS";
export const SET_TABLET = "DEVICE/SET_TABLET";
export const SET_QUICK_VIEW = "QUICK_VIEW/SET_QUICK_VIEW";
export const SET_CURRENT_UUID_QUICK_VIEW =
  "QIUCK_VIEW/SET_CURRENT_UUID_QUICK_VIEW";
export const SET_TO_WISH_LIST = "WISH/SET_TO_WISH_LIST";
export const ADD_TO_CART = "PRODUCT/ADD_TO_CART";
export const DELETE_FROM_CART = "PRODUCT/DELETE_FROM_CART";
export const UPDATE_PRODUCT_IN_CART = "PRODUCT/UPDATE_PRODUCT_IN_CART";
export const SET_CART_POPUP_STATUS = "CART/SET_CART_POPUP_STATUS";
