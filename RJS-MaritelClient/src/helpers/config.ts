export const FOOTER_INFO: FooterInfo[] = [
  {
    name: "Помощь",
    fields: [
      {
        name: "Размерная сетка",
        link: "/size-chart",
      },
      {
        name: "Список желаний",
        link: "/wish-list",
      },
      {
        name: "Мой аккаунт",
        link: "/my-profile",
      },
    ],
  },
  {
    name: "О нас",
    fields: [
      {
        name: "О бренде",
        link: "/about-brand",
      },
      {
        name: "История",
        link: "/history",
      },
      {
        name: "Блоггеры",
        link: "/bloggers",
      },
      {
        name: "Вакансии",
        link: "/vacancies",
      },
    ],
  },
  {
    name: "Правила",
    fields: [
      {
        name: "Доставка",
        link: "/delivery",
      },
      {
        name: "Оплата",
        link: "/payment",
      },
      {
        name: "Политика конфиденциальности",
        link: "/privacy-policy",
      },
      {
        name: "Договор публичной оферты",
        link: "/public-offer-agreement",
      },
    ],
  },
];

export const INFO_SLIDER: string[] = [
  "БЕСПЛАТНАЯ ДОСТАВКА ПРИ ЗАКАЗЕ НА СУММУ ОТ 1500 ГРН",
  "БЕСПЛАТНАЯ ДОСТАВКА ПРИ ЗАКАЗЕ НА СУММУ ОТ 1501 ГРН",
  "БЕСПЛАТНАЯ ДОСТАВКА ПРИ ЗАКАЗЕ НА СУММУ ОТ 1502 ГРН",
];

export const splitValue = " / ";
export const sortBy = "sortBy";

export const SIZES_CONFIG: string[] = [
  "XXS",
  "XS",
  "S",
  "M",
  "L",
  "XL",
  "XXL",
  "XXXL",
];

export const SHOES_SIZES_CONFIG: string[] = [
  "35",
  "36",
  "37",
  "38",
  "39",
  "40",
  "41",
  "42",
  "43",
  "44",
  "45",
  "46",
  "47",
];
