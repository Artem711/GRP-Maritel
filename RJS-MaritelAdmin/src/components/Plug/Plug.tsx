import React from "react";
import "./Plug.scss";

export const Plug = () => {
  return (
    <>
      <div className="Plug">
        <img src="images/plug/plug.svg" alt="plug" className="Plug__Img" />
      </div>
    </>
  );
};
