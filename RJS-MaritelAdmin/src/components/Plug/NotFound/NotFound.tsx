import React from "react";
import "./NotFound.scss";

export const NotFound = () => (
  <div className="NotFound">
    <img src="images/plug/404.svg" alt="not found" />
  </div>
);
