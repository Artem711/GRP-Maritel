import React from "react";
import "./LoadSpinner.scss";

export const LoadSpinner = () => (
  <div className="lds-ellipsis">
    <div></div>
    <div></div>
    <div></div>
    <div></div>
  </div>
);
